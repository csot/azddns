#!/bin/sh

set -e;

readonly get_ip_url="https://vpn.do.asnell.io/ip"

get_ip() {
    curl -6sS "${get_ip_url}" | tr -d '\r'
}

get_dns() {
    dig "${fqdn}" AAAA +short
}

add_new_record() {
    az network dns record-set aaaa add-record \
        --record-set-name "${hostname}" \
        --ipv6-address "${ip}" \
        --zone-name "${zone_name}" \
        --output table
}

remove_old_record() {
    az network dns record-set aaaa remove-record \
        --record-set-name "${hostname}" \
        --ipv6-address "${dns}" \
        --zone-name "${zone_name}" \
        --output table
}

update_if_needed() {
    readonly ip="$(get_ip)"
    readonly dns="$(get_dns)"

    if [ -z "${dns}" ]; then
        echo "creating DNS record. FQDN: ${fqdn}, New IP: ${ip}"
        add_new_record
    elif [ "${ip}" != "${dns}" ]; then
        echo "updating DNS. FQDN: ${fqdn}, Old IP: ${dns}, New IP: ${ip}"
        add_new_record
        remove_old_record
    else
        echo "DNS up-to-date. FQDN: ${fqdn}, IP: ${ip}"
    fi
}

print_usage() {
    echo "usage: $0 <hostname> <zone-name>" >&2
}

main() {
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        print_usage
        return
    elif [ 2 -ne $# ]; then
        print_usage
        exit 1
    fi

    readonly hostname="$1"
    readonly zone_name="$2"
    readonly fqdn="${hostname}.${zone_name}"
    update_if_needed
}

main "$@"
